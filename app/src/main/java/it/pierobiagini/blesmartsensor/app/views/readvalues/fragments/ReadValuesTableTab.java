package it.pierobiagini.blesmartsensor.app.views.readvalues.fragments;

/**
 * Created by Piero on 19/08/15.
 */
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import it.pierobiagini.blesmartsensor.app.R;
import it.pierobiagini.blesmartsensor.app.database.wrapper.ReadValue;

import java.util.List;

/**
 * Modella il fragment che permette la visualizzazione dei dati letti in forma tabulare
 */
public class ReadValuesTableTab extends Fragment {

    private LayoutInflater inflater;
    private ViewGroup container;
    private List<ReadValue> readValues;
    private TableLayout table;
    private TableLayout title;

    private static final int FONT_SIZE = 18;

    private static boolean rawValueCellVisible = true;
    private static boolean offsetCellVisible = true;
    private static boolean multiplierCellVisible = true;
    private static boolean timeCellVisible = true;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.inflater = inflater;
        this.container = container;
        View rootView = inflater.inflate(R.layout.fragment_read_values_table, container, false);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getView().getContext());
        rawValueCellVisible = settings.getBoolean(getString(R.string.show_raw_value_property), true);
        offsetCellVisible = settings.getBoolean(getString(R.string.show_offset_property), true);
        multiplierCellVisible = settings.getBoolean(getString(R.string.show_multiplier_property), true);
        timeCellVisible = settings.getBoolean(getString(R.string.show_time_property), true);

        title = (TableLayout) inflater.inflate(R.layout.table_read_values, container, false);
        table = (TableLayout) inflater.inflate(R.layout.table_read_values, container, false);
        load();
    }

    /**
     * Caricamento dei dati e creazione della view
     */
    private void load() {
        readValues = (List<ReadValue>) getArguments().getSerializable(getString(R.string.values));


        new Thread(){
            @Override
            public void run() {
                super.run();

                TableRow row = new TableRow(getView().getContext());
                //#
                TextView numberCell = new TextView(getView().getContext());
                numberCell.setGravity(Gravity.CENTER);
                numberCell.setText(R.string.number);
                numberCell.setTextSize(TypedValue.COMPLEX_UNIT_SP, FONT_SIZE);
                numberCell.setTypeface(null, Typeface.BOLD_ITALIC);
                row.addView(numberCell);

                TextView rawValueCell;
                if(rawValueCellVisible) {
                    //Raw value
                    rawValueCell = new TextView(getView().getContext());
                    rawValueCell.setGravity(Gravity.CENTER);
                    rawValueCell.setText(R.string.raw_value);
                    rawValueCell.setTextSize(TypedValue.COMPLEX_UNIT_SP, FONT_SIZE);
                    rawValueCell.setTypeface(null, Typeface.BOLD_ITALIC);
                    row.addView(rawValueCell);
                }

                TextView offsetCell;
                if(offsetCellVisible) {
                    //Offset
                    offsetCell = new TextView(getView().getContext());
                    offsetCell.setGravity(Gravity.CENTER);
                    offsetCell.setText(getString(R.string.offset));
                    offsetCell.setTypeface(null, Typeface.BOLD_ITALIC);
                    offsetCell.setTextSize(TypedValue.COMPLEX_UNIT_SP, FONT_SIZE);
                    row.addView(offsetCell);
                }

                TextView multiplierCell;
                if(multiplierCellVisible) {
                    //Multiplier
                    multiplierCell = new TextView(getView().getContext());
                    multiplierCell.setGravity(Gravity.CENTER);
                    multiplierCell.setText(getString(R.string.multiplier));
                    multiplierCell.setTypeface(null, Typeface.BOLD_ITALIC);
                    multiplierCell.setTextSize(TypedValue.COMPLEX_UNIT_SP, FONT_SIZE);
                    row.addView(multiplierCell);
                }

                //Value
                TextView valueCell = new TextView(getView().getContext());
                valueCell.setGravity(Gravity.CENTER);
                valueCell.setText(getString(R.string.value));
                valueCell.setTypeface(null, Typeface.BOLD_ITALIC);
                valueCell.setTextSize(TypedValue.COMPLEX_UNIT_SP, FONT_SIZE);
                row.addView(valueCell);

                TextView timeCell;
                if(timeCellVisible) {
                    //Time
                    timeCell = new TextView(getView().getContext());
                    timeCell.setGravity(Gravity.CENTER);
                    timeCell.setText(getString(R.string.time));
                    timeCell.setTypeface(null, Typeface.BOLD_ITALIC);
                    timeCell.setTextSize(TypedValue.COMPLEX_UNIT_SP, FONT_SIZE);
                    row.addView(timeCell);
                }

                table.addView(row, new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

                int i = 0;
                for (ReadValue readValue : readValues) {
                    i++;
                    row = new TableRow(getView().getContext());
                    row.setPadding(10, 0, 10, 0);

                    //#
                    numberCell = new TextView(getView().getContext());
                    numberCell.setGravity(Gravity.CENTER);
                    numberCell.setTypeface(null, Typeface.ITALIC);
                    numberCell.setText(Integer.toString(i));
                    row.addView(numberCell);

                    if(rawValueCellVisible) {
                        //Raw value
                        rawValueCell = new TextView(getView().getContext());
                        rawValueCell.setGravity(Gravity.CENTER);
                        rawValueCell.setText(Integer.toString(readValue.getValue()));
                        row.addView(rawValueCell);
                    }

                    if(offsetCellVisible) {
                        //Offset
                        offsetCell = new TextView(getView().getContext());
                        offsetCell.setGravity(Gravity.CENTER);
                        offsetCell.setText(Double.toString(readValue.getOffset()));
                        row.addView(offsetCell);
                    }

                    if(multiplierCellVisible) {
                        //Multiplier
                        multiplierCell = new TextView(getView().getContext());
                        multiplierCell.setGravity(Gravity.CENTER);
                        multiplierCell.setText(Double.toString(readValue.getMultiplier()));
                        row.addView(multiplierCell);
                    }

                    //Value
                    valueCell = new TextView(getView().getContext());
                    valueCell.setGravity(Gravity.CENTER);
                    valueCell.setText("  " + Double.toString(readValue.getValue() * readValue.getMultiplier() + readValue.getOffset()));
                    row.addView(valueCell);

                    if(timeCellVisible) {
                        //Time
                        timeCell = new TextView(getView().getContext());
                        timeCell.setGravity(Gravity.CENTER);
                        timeCell.setText(Double.toString((double) readValue.getTimeFromConnection() / (double) 1000));
                        row.addView(timeCell);
                    }

                    table.addView(row, new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
                }
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        LinearLayout container = ((LinearLayout) getView().findViewById(R.id.layout_container));
                        container.removeAllViews();
                        container.addView(table);
                        getView().findViewById(R.id.loading_panel).setVisibility(View.GONE);
                    }
                });
            }
        }.start();
    }
}
