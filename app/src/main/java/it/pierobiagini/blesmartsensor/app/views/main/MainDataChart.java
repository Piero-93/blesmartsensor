package it.pierobiagini.blesmartsensor.app.views.main;

import android.content.Context;
import android.graphics.Paint;
import android.widget.LinearLayout;
import it.pierobiagini.blesmartsensor.app.R;
import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

/**
 * Modella il grafico visualizzato nella view principale
 */
public final class MainDataChart {

    private final static double TIME_INTERVAL = 0.5;

    private static XYMultipleSeriesDataset dataSet = new XYMultipleSeriesDataset();
    private static XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
    private static XYSeries dataSeries;
    private static XYSeriesRenderer currentRenderer;
    private static GraphicalView chart;
    private static double counter;

    /**
     * Inizializzazione del grafico
     * @param context context della view in cui dovrà essere inserito il grafico
     */
    public static void initialize(Context context) {
        dataSeries = new XYSeries("Data");
        dataSet.addSeries(dataSeries);
        currentRenderer = new XYSeriesRenderer();
        renderer.addSeriesRenderer(currentRenderer);

        //Graphical details
        currentRenderer.setPointStyle(PointStyle.DIAMOND);
        currentRenderer.setFillPoints(true);
        currentRenderer.setLineWidth(5);
        renderer.setPointSize(10);
        renderer.setMarginsColor(context.getResources().getColor(R.color.background_material_light));
        renderer.setBackgroundColor(context.getResources().getColor(R.color.background_material_dark));
        renderer.setLabelsTextSize(40);
        renderer.setAxisTitleTextSize(30);
        renderer.setYTitle("Value");
        renderer.setYAxisAlign(Paint.Align.LEFT, 0);
        renderer.setYLabelsAlign(Paint.Align.RIGHT, 0);
        renderer.setYLabelsAngle(90);
        renderer.setYLabelsColor(0, context.getResources().getColor(R.color.abc_primary_text_material_light));
        renderer.setXTitle("Time (s)");
        renderer.setXLabelsColor(context.getResources().getColor(R.color.abc_primary_text_material_light));
        renderer.setGridColor(context.getResources().getColor(R.color.background_floating_material_dark));
        renderer.setLabelsColor(context.getResources().getColor(R.color.abc_primary_text_material_light));
        renderer.setShowGrid(true);
        renderer.setZoomEnabled(false, true);
        renderer.setPanEnabled(false);
    }

    /**
     * Sostituzione del grafico al {@link LinearLayout}
     * @param layout {@link LinearLayout} da sostituire
     */
    public static void setChartInLayout(LinearLayout layout) {
        if(chart == null) {
            MainDataChart.initialize(layout.getContext());
            chart = ChartFactory.getLineChartView(layout.getContext(), dataSet, renderer);
            layout.addView(chart);
        } else {
            chart.repaint();
        }
    }

    /**
     * Aggiunta dei dati nel grafico
     * @param data valore da aggiungere
     * @param multiplier valore del moltiplicatore
     * @param offset valore di scostamento
     * @param elapsedTime tempo trascorso dalla connessione
     */
    public static void addData(int data, double multiplier, double offset, int elapsedTime) {
        dataSeries.add(counter, data * multiplier + offset);
        counter += ((elapsedTime/1000) - counter) * TIME_INTERVAL;
        renderer.setXAxisMin(counter - 10);
        renderer.setXAxisMax(counter + 2);
        chart.repaint();
    }

    /**
     * Svuota il grafico
     */
    public static void reset() {
        dataSeries.clear();
        counter = 0;
    }
}
