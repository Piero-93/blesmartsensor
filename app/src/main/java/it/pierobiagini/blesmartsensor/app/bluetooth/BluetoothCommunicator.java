package it.pierobiagini.blesmartsensor.app.bluetooth;

import android.app.Service;
import android.bluetooth.*;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.UUID;

/**
 * Modella il BluetoothCommunicator, ovvero l'oggetto che si occupa della gestione della parte Bluetooth:
 *  - Connessione e disconnessione
 *  - Notifica di ricezione dei dati
 *  - Invio di dati
 *  - Pairing
 *  - ...
 *
 *  @autohr Biagini Piero
 */
public class BluetoothCommunicator extends Service {

    //Stati possibili
    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_CONNECTED = 1;

    //Azioni
    public final static String ACTION_GATT_CONNECTED = "ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED = "ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICE_DISCOVERED = "ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE= "ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA = "EXTRA DATA";

    //Riferimenti alle caratteristiche da leggere/scrivere
        //Servizio contenente le caratteristiche
    public final static UUID SERVICE_UUID = UUID.fromString("0000fff0-0000-1000-8000-00805f9b34fb");
        //Caratteristica su cui vengono ricevuti i dati letti
    public final static UUID CHARACTERISTIC_UUID_NOTIFY = UUID.fromString("0000fff4-0000-1000-8000-00805f9b34fb");
    //public final static UUID CHARACTERISTIC_UUID_READ = UUID.fromString("0000fff5-0000-1000-8000-00805f9b34fb");
        //Caratteristica su cui viene scritto l'intervallo di sincronizzazione
    public final static UUID CHARACTERISTIC_UUID_SYNC_INTERVAL = UUID.fromString("0000fff1-0000-1000-8000-00805f9b34fb");

    //Riferimento al GATT
    private static BluetoothGatt bluetoothGatt;

    //Stato della connessione
    private static int connectionState;

    //Callback del GATT
    public final BluetoothGattCallback bluetoothGattCallback = new BluetoothGattCallback() {
        /**
         * Lettura di un valore di una caratteristica
         * @param gatt riferimento al GATT
         * @param characteristic caratteristica il cui valore è stato letto
         * @param status stato
         */
        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic, int status) {
            if(status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            }
        }

        /**
         * Connessione o disconnessione di un device
         * @param gatt riferimento al GATT
         * @param status stato
         * @param newState nuovo stato (connected o disconnected)
         */
        @Override
        public void onConnectionStateChange(final BluetoothGatt gatt, final int status, final int newState) {
            if(newState == BluetoothProfile.STATE_CONNECTED) {
                connectionState = STATE_CONNECTED;
                broadcastUpdate(ACTION_GATT_CONNECTED);
                bluetoothGatt.discoverServices();

            } else if(newState == BluetoothProfile.STATE_DISCONNECTED) {
                String intentAction = ACTION_GATT_DISCONNECTED;
                gatt.close();
                connectionState = STATE_DISCONNECTED;
                broadcastUpdate(intentAction);
            }
        }

        /**
         * Rilevamento dei services
         * @param gatt riferimento al GATT
         * @param status stato del servizio
         */
        @Override
        public void onServicesDiscovered(final BluetoothGatt gatt, final int status) {
            if(status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_GATT_SERVICE_DISCOVERED);
                BluetoothGattCharacteristic notifyCharacteristic = gatt.getService(SERVICE_UUID).getCharacteristic(CHARACTERISTIC_UUID_NOTIFY);
                gatt.setCharacteristicNotification(notifyCharacteristic, true);
                BluetoothGattDescriptor descriptor = notifyCharacteristic.getDescriptor(CHARACTERISTIC_UUID_NOTIFY);
                descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                gatt.writeDescriptor(descriptor);
            }
        }

        /**
         * Ricezione di una notifica
         * @param gatt riferimento al GATT
         * @param characteristic caratteristica il cui valore è cambiato
         */
        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
        }
    };

    /**
     * Invio di un messaggio broadcast per segnalare l'esecuzione di un'azione
     * @param action azione eseguita
     */
    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    /**
     * Invio di un messaggio broadcast per segnalare l'esecuzione di un'azione e i dati ottenuti da tale azione
     * @param action azione eseguita
     * @param characteristic riferimento alla caratteristica
     */
    private void broadcastUpdate(final String action, final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);

        if(CHARACTERISTIC_UUID_NOTIFY.equals(characteristic.getUuid())) {
            int readData = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, 0);
            intent.putExtra(EXTRA_DATA, readData);
        } else {
            Log.w("Wrong UUID", characteristic.getUuid().toString());
        }

        sendBroadcast(intent);
    }

    /**
     * Invio dell'intervallo di trasmissione desiderato
     * @param interval valore dell'intervallo di trasmissione
     */
    public static void writeDesiredInterval(int interval) {
        BluetoothGattCharacteristic characteristic = bluetoothGatt.getService(SERVICE_UUID).getCharacteristic(CHARACTERISTIC_UUID_SYNC_INTERVAL);
        characteristic.setValue(interval, BluetoothGattCharacteristic.FORMAT_UINT8, 0);
        boolean status = bluetoothGatt.writeCharacteristic(characteristic);
    }

    /**
     * Ottenimento dello stato del GATT
     * @return lo stato del GATT
     */
    public static int getState() {
        return connectionState;
    }

    /**
     * Ottenimento del nome del dispositivo connesso
     * @return il nome del dispositivo connesso
     */
    public static String getConnectedDeviceName() {
        return bluetoothGatt.getConnectedDevices().get(0).getName();
    }

    /**
     * Disconnessione del GATT
     */
    public static void disconnect() {
        if(bluetoothGatt != null) {
            bluetoothGatt.close();
            bluetoothGatt = null;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("New Service", "Starting the communication service");
        BluetoothDevice device = (BluetoothDevice) intent.getExtras().get("Device");
        Log.i("Connecting to", device.getName());
        bluetoothGatt = device.connectGatt(null, false, bluetoothGattCallback);
        return super.onStartCommand(intent, flags, startId);
    }
}