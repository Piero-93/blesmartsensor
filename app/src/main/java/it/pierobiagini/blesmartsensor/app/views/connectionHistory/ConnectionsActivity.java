package it.pierobiagini.blesmartsensor.app.views.connectionhistory;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import it.pierobiagini.blesmartsensor.app.R;
import it.pierobiagini.blesmartsensor.app.database.DatabaseAdapter;
import it.pierobiagini.blesmartsensor.app.database.wrapper.Connection;
import it.pierobiagini.blesmartsensor.app.database.wrapper.MyDate;
import it.pierobiagini.blesmartsensor.app.views.AppCompatActivityWithColouredStatusBar;
import it.pierobiagini.blesmartsensor.app.views.settings.SettingsActivity;
import it.pierobiagini.blesmartsensor.app.views.readvalues.ReadValuesActivity;

import java.sql.SQLException;

/**
 * Modella la view per la visualizzazione della cronologia delle connessioni
 */
public class ConnectionsActivity extends AppCompatActivityWithColouredStatusBar {

    private ConnectionsAdapter listViewAdapter;

    private final DatabaseAdapter dbAdapter = new DatabaseAdapter(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connections);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        setStatusBarColor();

        listViewAdapter = new ConnectionsAdapter(this);

        final ListView listView = (ListView) findViewById(R.id.listConnections);
        listView.setAdapter(listViewAdapter);

        //Click breve su un elemento della lista (apertura)
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final Connection pressed = (Connection) listViewAdapter.getItem(i);
                if (pressed != null) {
                    Intent intent = new Intent(ConnectionsActivity.this, ReadValuesActivity.class);
                    intent.putExtra(dbAdapter.VALUES_PERIPHERAL_ID, pressed.getPeripheralID());
                    intent.putExtra(dbAdapter.VALUES_TIMESTAMP, pressed.getTimestamp());
                    startActivity(intent);
                }
            }
        });

        //Click lungo su un elemento della lista (cancellazione)
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int i, long l) {
                final Connection pressed = (Connection) listViewAdapter.getItem(i);
                if (pressed != null) {
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int pressedButton) {
                            MyDate currentConnection = (MyDate) getIntent().getSerializableExtra(getString(R.string.connection_time));
                            if(pressedButton == DialogInterface.BUTTON_POSITIVE) {
                                if (currentConnection == null || !pressed.getTimestamp().equals(currentConnection)) {
                                    if (dbAdapter.deleteConnetionAndRelatedValues(pressed.getPeripheralID(), pressed.getTimestamp())) {
                                        listViewAdapter.clear();
                                        loadData();
                                        listViewAdapter.notifyDataSetChanged();
                                    }
                                } else {
                                    Toast.makeText(ConnectionsActivity.this, R.string.error_current_connection, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(ConnectionsActivity.this);
                    builder.setMessage(R.string.delete_message);
                    builder.setTitle(R.string.delete_title);
                    builder.setPositiveButton(R.string.yes, dialogClickListener);
                    builder.setNegativeButton(R.string.no, dialogClickListener);
                    builder.show();
                }
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        listViewAdapter.clear();
        loadData();
        listViewAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_connections, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent settings = new Intent(this, SettingsActivity.class);
            startActivity(settings);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Caricamento dei dati dal database
     */
    private void loadData() {
        try {
            dbAdapter.open();
            Cursor connections;
            connections = dbAdapter.fetchAllConnections();
            while(connections.moveToNext()) {
                listViewAdapter.addItem(new Connection(connections.getString(0), connections.getString(1), connections.getString(2)));
            }
        } catch (SQLException e) {
            Toast.makeText(this, R.string.db_connection_error, Toast.LENGTH_LONG);
        }
    }
}
