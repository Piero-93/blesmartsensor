package it.pierobiagini.blesmartsensor.app.views.main;

import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.*;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import it.pierobiagini.blesmartsensor.app.R;
import it.pierobiagini.blesmartsensor.app.bluetooth.BluetoothCommunicator;
import it.pierobiagini.blesmartsensor.app.bluetooth.BluetoothScanner;
import it.pierobiagini.blesmartsensor.app.database.DatabaseAdapter;
import it.pierobiagini.blesmartsensor.app.database.wrapper.MyDate;
import it.pierobiagini.blesmartsensor.app.views.AppCompatActivityWithColouredStatusBar;
import it.pierobiagini.blesmartsensor.app.views.connectionhistory.ConnectionsActivity;
import it.pierobiagini.blesmartsensor.app.views.scan.ScanActivity;
import it.pierobiagini.blesmartsensor.app.views.settings.SettingsActivity;

import java.sql.SQLException;
import java.util.Date;

/**
 * Modella la view principale
 */
public class MainActivity extends AppCompatActivityWithColouredStatusBar implements SharedPreferences.OnSharedPreferenceChangeListener {

    //Codici per "startActivityForResult"
    public static final int REQUEST_DISCOVER_BLUETOOTH = 0;
    private final static int REQUEST_BLUETOOTH_ENABLE = 1;
    private final static int REQUEST_BLUETOOTH_BONDING = 2;

    private BluetoothDevice connectedDevice;
    private MyDate connectionStart;
    private AlertDialog connectionAlert;
    private boolean connected;
    private int interval;

    //Valori di correzione
    private double offset;
    private double multiplier;

    //Abilitazione o disabilitazione della registrazione
    private boolean record = true;

    //Database
    private final DatabaseAdapter dbAdapter = new DatabaseAdapter(this);

    //Listener dei messaggi broadcast
    private final BroadcastReceiver gattUpdaterReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if(BluetoothCommunicator.ACTION_GATT_CONNECTED.equals(action)) {
                manageConnection();

            } else if(BluetoothCommunicator.ACTION_GATT_DISCONNECTED.equals(action)) {
                manageDisconnection();

            } else if(BluetoothCommunicator.ACTION_GATT_SERVICE_DISCOVERED.equals(action)) {
                //DO NOTHING

            } else if(BluetoothCommunicator.ACTION_DATA_AVAILABLE.equals(action)) {
                int value = (int) intent.getExtras().get(BluetoothCommunicator.EXTRA_DATA);
                elaborateReceivedData(value);
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        LinearLayout layout = (LinearLayout) findViewById(R.id.chart);
        MainDataChart.setChartInLayout(layout);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        setStatusBarColor();

        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
        updateProperties();

        try {
            dbAdapter.open();
        } catch (SQLException e) {
            Toast.makeText(this, "Cannot connect to DB!", Toast.LENGTH_LONG);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent settings = new Intent(this, SettingsActivity.class);
            startActivity(settings);
            return true;
        } else if (id == R.id.action_scan) {
            createBluetoothEnablingRequest();
        } else if (id == R.id.action_connectionHistory) {
            Intent connectionHistory = new Intent(this, ConnectionsActivity.class);
            if (connected) {
                connectionHistory.putExtra(getString(R.string.connection_time), connectionStart);
            }
            startActivity(connectionHistory);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_DISCOVER_BLUETOOTH && resultCode == RESULT_OK) {
            BluetoothDevice device = (BluetoothDevice) data.getExtras().get(getString(R.string.device_extra_intent));
            connectedDevice = device;
            if(record) {
                connectionStart = new MyDate();
                dbAdapter.createConnection(device.getAddress(), device.getName(), connectionStart);
            }
            bondAndStartService();
        } else if(requestCode == REQUEST_BLUETOOTH_ENABLE) {
            if(resultCode == RESULT_OK) {
                Toast.makeText(this, R.string.turning_on_bluetooth, Toast.LENGTH_SHORT).show();
            } else if(resultCode == RESULT_CANCELED) {
                createBluetoothEnablingRequest();
            }
        } else if(requestCode == REQUEST_BLUETOOTH_BONDING) {
            bondAndStartService();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopBleService();
    }

    @Override
    public void onBackPressed() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbAdapter.close();
    }

    /**
     * Visualizzazione del messaggio per l'esecuzione del pairing
     */
    private void showBondingAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.pairing_message));
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                createBluetoothBondingRequest();
            }
        });
        builder.show();

    }

    /**
     * Lancio della schermata per l'abilitazione del Bluetooth
     */
    private void createBluetoothEnablingRequest() {
        Intent scanBluetooth = new Intent(MainActivity.this, ScanActivity.class);
        startActivityForResult(scanBluetooth, REQUEST_DISCOVER_BLUETOOTH);
    }

    /**
     * Lancio della schermata per l'esecuzione del pairing
     */
    private void createBluetoothBondingRequest() {
        Intent openBluetoothSettings = new Intent().setAction(Settings.ACTION_BLUETOOTH_SETTINGS);
        startActivityForResult(openBluetoothSettings, REQUEST_BLUETOOTH_BONDING);
    }

    /**
     * Pairing, connessione e registrazione dei broadcast receiver
     */
    private void bondAndStartService() {
        if (connectedDevice.getBondState() == BluetoothDevice.BOND_NONE) {
            showBondingAlert();
            return;
        }

        showConnectingAlert(connectedDevice.getName());

        Intent startCommunication = new Intent(this, BluetoothCommunicator.class);
        startCommunication.putExtra(getString(R.string.device_extra_intent), connectedDevice);
        startService(startCommunication);

        registerReceiver(gattUpdaterReceiver, new IntentFilter(BluetoothCommunicator.ACTION_GATT_CONNECTED));
        registerReceiver(gattUpdaterReceiver, new IntentFilter(BluetoothCommunicator.ACTION_GATT_DISCONNECTED));
        registerReceiver(gattUpdaterReceiver, new IntentFilter(BluetoothCommunicator.ACTION_GATT_SERVICE_DISCOVERED));
        registerReceiver(gattUpdaterReceiver, new IntentFilter(BluetoothCommunicator.ACTION_DATA_AVAILABLE));
    }

    /**
     * Visualizzazione di un alert contenente il nome della periferica a cui ci si sta connettendo
     * @param name nome della periferica
     */
    private void showConnectingAlert(String name) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage(getString(R.string.connecting_to) + name);
        connectionAlert = builder.show();
    }

    /**
     * Gestione dell'evento di connessione
     */
    private void manageConnection() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setConnectedDeviceName();
                updateRecordSettings();
                writeSyncInterval();
                MainDataChart.reset();
            }
        });
    }

    /**
     * Visualizzazione su schermo del nome del dispositivo connesso
     */
    private void setConnectedDeviceName() {
        TextView connectedDeviceTextView = (TextView) findViewById(R.id.textConnectedDevice);
        connectedDeviceTextView.setText(connectedDevice.getName());
        connectionAlert.dismiss();
        connected = true;
    }

    /**
     * Gestione dell'evento di disconnessione
     */
    private void manageDisconnection() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView connectedDevice = (TextView) findViewById(R.id.textConnectedDevice);
                connectedDevice.setText(R.string.no_device_connected);
                connected = false;
                stopBleService();
                if (!BluetoothScanner.checkBluetoothTurnedOn()) {
                    createBluetoothEnablingRequest();
                }
                Toast.makeText(MainActivity.this, R.string.device_disconnected, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Disattivazione del service
     */
    private void stopBleService() {
        Intent stopCommunication = new Intent(MainActivity.this, BluetoothCommunicator.class);
        stopService(stopCommunication);
    }

    /**
     * Elaborazione dei dati ricevuti (visualizzazione su schermo e salvataggio)
     * @param data valore ricevuto
     */
    private void elaborateReceivedData(final int data) {
        if(connected) {
            final int elapsedTime = (int) (new Date().getTime() - connectionStart.getTime());
            dbAdapter.createValue(connectedDevice.getAddress(), connectionStart, data, offset, multiplier, elapsedTime);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    TextView receivedData = (TextView) findViewById(R.id.textReceivedData);
                    TextView rawReceivedData = (TextView) findViewById(R.id.textRawReceivedData);
                    double correctedData = data * multiplier + offset;
                    receivedData.setText(Double.toString(correctedData));
                    rawReceivedData.setText(Integer.toString(data));
                    MainDataChart.addData(data, multiplier, offset, elapsedTime);
                    connected = true;
                    }
            });
        }
    }

    /**
     * Lettura delle shared preferences
     */
    private void updateProperties() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);

        offset = Double.parseDouble(settings.getString(getString(R.string.offset_value_property), "0"));
        multiplier = Double.parseDouble(settings.getString(getString(R.string.multiplier_value_property), "1.0"));

        boolean showRawDataInView = settings.getBoolean(getString(R.string.show_raw_text_property), true);

        TextView rawDataLabel = (TextView) findViewById(R.id.labelRawReceivedData);
        TextView rawDataValue = (TextView) findViewById(R.id.textRawReceivedData);

        if(showRawDataInView) {
            rawDataLabel.setVisibility(View.VISIBLE);
            rawDataValue.setVisibility(View.VISIBLE);
        } else {
            rawDataLabel.setVisibility(View.GONE);
            rawDataValue.setVisibility(View.GONE);
        }
    }

    /**
     * Lettura della shared preferences riguardante la registrazione
     */
    private void updateRecordSettings() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        record = settings.getBoolean(getString(R.string.record_settings_property), true);
    }

    /**
     * Scrittura dell'intervallo di trasmissione
     */
    private void writeSyncInterval() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        String syncRate = settings.getString(getString(R.string.sync_rate_property), "1");

        if (syncRate != null && !syncRate.equals("")) {
            int value =  Integer.parseInt(syncRate);
            interval = value;
            if(connected) {
                BluetoothCommunicator.writeDesiredInterval(value);
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {

        if(s.compareTo(getString(R.string.sync_rate_property)) == 0) {
            String syncRate = sharedPreferences.getString(getString(R.string.sync_rate_property), "1");

            if (syncRate != null && !syncRate.equals("")) {
                int value = Integer.parseInt(syncRate);
                interval = value;
                if (connected) {
                    BluetoothCommunicator.writeDesiredInterval(value);
                }
            }
        }

        if(s.compareTo(getString(R.string.offset_value_property)) == 0) {
            offset = Double.parseDouble(sharedPreferences.getString(getString(R.string.offset_value_property), "0"));
        }

        if(s.compareTo(getString(R.string.multiplier_value_property)) == 0) {
            multiplier = Double.parseDouble(sharedPreferences.getString(getString(R.string.multiplier_value_property), "1.0"));
        }

        if(s.compareTo(getString(R.string.show_raw_text_property)) == 0) {
            boolean showRawDataInView = sharedPreferences.getBoolean(getString(R.string.show_raw_text_property), true);

            TextView rawDataLabel = (TextView) findViewById(R.id.labelRawReceivedData);
            TextView rawDataValue = (TextView) findViewById(R.id.textRawReceivedData);

            if(showRawDataInView) {
                rawDataLabel.setVisibility(View.VISIBLE);
                rawDataValue.setVisibility(View.VISIBLE);
            } else {
                rawDataLabel.setVisibility(View.GONE);
                rawDataValue.setVisibility(View.GONE);
            }
        }
    }

}
