package it.pierobiagini.blesmartsensor.app.views.readvalues.fragments;

import android.support.annotation.Nullable;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.*;
import android.widget.Button;
import android.widget.LinearLayout;

import it.pierobiagini.blesmartsensor.app.R;
import it.pierobiagini.blesmartsensor.app.database.wrapper.ReadValue;
import it.pierobiagini.blesmartsensor.app.views.readvalues.utils.HistoryDataChart;

import java.util.List;

/**
 * Modella il fragment che permette la visualizzazione dei dati letti su grafico
 */
public class ReadValuesChartTab extends Fragment {

    HistoryDataChart chart;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_read_values_chart, container, false);

        Button center = (Button) rootView.findViewById(R.id.button_center);
        center.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chart.center();
            }
        });

        Button verticalZoomIn = (Button) rootView.findViewById(R.id.plus_vertical);
        verticalZoomIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("asdasd");
                chart.verticalZoomIn();
            }
        });

        Button verticalZoomOut = (Button) rootView.findViewById(R.id.minus_vertical);
        verticalZoomOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chart.verticalZoomOut();
            }
        });

        Button horizontalZoomIn = (Button) rootView.findViewById(R.id.plus_horizontal);
        horizontalZoomIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chart.horizontalZoomIn();
            }
        });

        Button horizontalZoomOut = (Button) rootView.findViewById(R.id.minus_horizontal);
        horizontalZoomOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chart.horizontalZoomOut();
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        LinearLayout layout = (LinearLayout) getView().findViewById(R.id.chart);
        chart = new HistoryDataChart();
        chart.setChartInLayout(layout);

        List<ReadValue> readValues = (List<ReadValue>) getArguments().getSerializable("Values");
        for (ReadValue readValue : readValues) {
            chart.addData(readValue.getValue(), readValue.getMultiplier(), readValue.getOffset(), readValue.getTimeFromConnection());
        }
        chart.center();
    }
}
