package it.pierobiagini.blesmartsensor.app.views.settings;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import it.pierobiagini.blesmartsensor.app.R;

/**
 * Modella la view per la visualizzazione delle impostazioni
 */
public class SettingsActivity extends PreferenceActivity {

    /**
     * Impostazione del colore della status bar (da eseguire solo su Lollipop)
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setStatusBarColor() {
        getWindow().setStatusBarColor(getResources().getColor(R.color.dark_master));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LinearLayout root = (LinearLayout)findViewById(android.R.id.list).getParent().getParent().getParent();
        Toolbar bar = (Toolbar) LayoutInflater.from(this).inflate(R.layout.tool_bar_settings, root, false);
        root.addView(bar, 0);
        bar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        setStatusBarColor();

        addPreferencesFromResource(R.xml.preferences);
    }
}