package it.pierobiagini.blesmartsensor.app.views.readvalues;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import it.pierobiagini.blesmartsensor.app.database.wrapper.ReadValue;
import it.pierobiagini.blesmartsensor.app.views.readvalues.fragments.ReadValuesChartTab;
import it.pierobiagini.blesmartsensor.app.views.readvalues.fragments.ReadValuesTableTab;

import java.io.Serializable;
import java.util.List;

/**
 * Modella un ViewPagerAdapter in grado di contenere i fragment di visualizzazione grafica e tabulare.
 * Codice tratto da http://www.android4devs.com/2015/01/how-to-make-material-design-sliding-tabs.html e opportunamente modificato.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    //Titoli delle tab
    private CharSequence Titles[];

    //Numero di tab
    private int NumbOfTabs;

    //Valori da visualizzare
    private List<ReadValue> fetchedValues;


    /**
     * Creazione dell'adapter
     * @param fm fragment manager
     * @param mTitles vettore contenente i titoli delle tabs
     * @param mNumbOfTabsumb numero delle tab
     * @param list lista di valori da visualizzare
     */
    public ViewPagerAdapter(FragmentManager fm,CharSequence mTitles[], int mNumbOfTabsumb, List<ReadValue> list) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;

        this.fetchedValues = list;


    }

    @Override
    public Fragment getItem(int position) {

        Bundle bundle = new Bundle();
        bundle.putSerializable("Values", (Serializable) fetchedValues);
        if(position == 0) //Prima tab
        {
            ReadValuesChartTab tab1 = new ReadValuesChartTab();
            tab1.setArguments(bundle);
            return tab1;
        }
        else //Seconda tab
        {
            ReadValuesTableTab tab2 = new ReadValuesTableTab();
            tab2.setArguments(bundle);
            return tab2;
        }


    }

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    @Override
    public int getCount() {
        return NumbOfTabs;
    }


}
