package it.pierobiagini.blesmartsensor.app.database.wrapper;

import java.util.UnknownFormatConversionException;

/**
 * Modella un record del database della tabella "Connections"
 */
public class  Connection {
    private String peripheralID = "";
    private String peripheralName = "";
    private MyDate timestamp = new MyDate();

    /**
     * Creazione di una connessione inizializzando tutti i valori manualmente
     * @param peripheralID id della periferica
     * @param peripheralName nome della periferica
     * @param timestamp oggetto di tipo {@link String} rappresentante data e ora di inizio
     */
    public Connection(String peripheralID, String peripheralName, String timestamp) {
        this.peripheralID = peripheralID;
        this.peripheralName = peripheralName;
        if(MyDate.isValidTimestamp(timestamp)) {
            this.timestamp = MyDate.convertStringTimestampToDate(timestamp);
        } else {
            throw new UnknownFormatConversionException("Invalid Timestamp");
        }
    }

    /**
     * Creazione di una connessione inizializzando tutti i valori manualmente
     * @param peripheralID id della periferica
     * @param peripheralName nome della periferica
     * @param timestamp oggetto di tipo {@link MyDate} rappresentante data e ora di inizio
     */
    public Connection(String peripheralID, String peripheralName, MyDate timestamp) {
        this.peripheralID = peripheralID;
        this.peripheralName = peripheralName;
        this.timestamp = timestamp;
    }

    /**
     * Creazione di una connessione inizializzando tutti i valori manualmente
     * @param peripheralID id della periferica
     * @param peripheralName nome della periferica
     * @param timestamp variabile di tipo primitivo long rappresentante data e ora di inizio
     */
    public Connection(String peripheralID, String peripheralName, long timestamp) {
        this.peripheralID = peripheralID;
        this.peripheralName = peripheralName;
        this.timestamp.setTime(timestamp);
    }

    /**
     * Ottenimento dell'id della periferica
     * @return l'id della periferica
     */
    public String getPeripheralID() {
        return peripheralID;
    }

    /**
     * Ottenimento del nome della periferica
     * @return il nome della periferica
     */
    public String getPeripheralName() {
        return peripheralName;
    }

    /**
     * Ottenimento della data di inizio della connessione
     * @return la data di inizio della connessione
     */
    public MyDate getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "Connection{" +
                "peripheralID='" + peripheralID + '\'' +
                ", peripheralName='" + peripheralName + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}