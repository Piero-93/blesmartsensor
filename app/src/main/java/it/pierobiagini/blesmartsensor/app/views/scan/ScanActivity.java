package it.pierobiagini.blesmartsensor.app.views.scan;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import it.pierobiagini.blesmartsensor.app.R;
import it.pierobiagini.blesmartsensor.app.bluetooth.BluetoothScanner;
import it.pierobiagini.blesmartsensor.app.bluetooth.BluetoothScannerObserver;
import it.pierobiagini.blesmartsensor.app.views.AppCompatActivityWithColouredStatusBar;

/**
 * Modella la view per la visualizzazione dei dispositivi rilevati dalla scansione
 */
public class ScanActivity extends AppCompatActivityWithColouredStatusBar implements BluetoothScannerObserver {

    private BluetoothDevicesAdapter listViewAdapter;

    private final static int REQUEST_BLUETOOTH_ENABLE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        setStatusBarColor();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        listViewAdapter = new BluetoothDevicesAdapter(this);

        ListView listView = (ListView) findViewById(R.id.LIST_SCAN);
        listView.setAdapter(listViewAdapter);

        //Click su un elemento della lista
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //BluetoothScanner.connect(((BluetoothDevice) adapterView.getItemAtPosition(i)));
                Log.i("Connecting to", ((BluetoothDevice) adapterView.getItemAtPosition(i)).getName());
                Intent result = new Intent();
                result.putExtra("Device", (BluetoothDevice) adapterView.getItemAtPosition(i));
                setResult(RESULT_OK, result);
                finish();
            }
        });

        Log.i("Initializing", "Initializing Bluetooth Communicator");
        initializeBluetoothCommunicator();

        BluetoothScanner.setScanner(this);
        BluetoothScanner.scan();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_scan, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_scan) {
            listViewAdapter.clear();
            BluetoothScanner.scan();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void deviceFound(BluetoothDevice device) {
        listViewAdapter.addItem(device);
        listViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void scanningFinished() {
        RelativeLayout progressSpinner = (RelativeLayout) findViewById(R.id.loadingPanel);
        progressSpinner.setVisibility(View.GONE);

        try {
            if (listViewAdapter.getCount() == 0) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder alert = new AlertDialog.Builder(ScanActivity.this);
                        alert.setMessage(getString(R.string.no_device_found));
                        alert.setPositiveButton(getString(android.R.string.ok), null);
                        alert.show();
                    }
                });
            }
        } catch (WindowManager.BadTokenException ignored) {}
    }

    @Override
    public void scanningStarted() {
        RelativeLayout progressSpinner = (RelativeLayout) findViewById(R.id.loadingPanel);
        progressSpinner.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQUEST_BLUETOOTH_ENABLE) {
            if(resultCode == RESULT_OK) {
                Toast.makeText(this, R.string.turning_on_bluetooth, Toast.LENGTH_SHORT).show();
            } else if(resultCode == RESULT_CANCELED) {
                finish();
                //initializeBluetoothCommunicator();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        BluetoothScanner.stopScan();
    }

    /**
     * Inizializzazione del BluetoothCommunicator
     */
    private void initializeBluetoothCommunicator() {
        if(!BluetoothScanner.initialize((BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE))) {
            Intent enableBluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetoothIntent, REQUEST_BLUETOOTH_ENABLE);
        }
    }
}
