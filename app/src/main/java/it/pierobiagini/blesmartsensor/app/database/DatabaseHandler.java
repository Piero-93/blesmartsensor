package it.pierobiagini.blesmartsensor.app.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Modella il database in cui vengono memorizzate le connessioni e i valori letti
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    //Versione del database
    private static final int DATABASE_VERSION = 1;

    //Nome del database
    private static final String DATABASE_NAME = "BLESmartSensor.db";

    //Query di creazione delle tabelle
    private static final String QUERY_CREATE_CONNECTIONS = "create table Connections (\n" +
            "     PeripheralID varchar(12) not null,\n" +
            "     PeripheralName varchar(50) not null,\n" +
            "     Timestamp varchar(10) not null,\n" +
            "     constraint IDConnection primary key (PeripheralID, Timestamp));";

    private static final String QUERY_CREATE_VALUES = "create table ReadValues (\n" +
            "     PeripheralID varchar(12) not null,\n" +
            "     Timestamp varchar(10) not null,\n" +
            "     Value numeric(10) not null,\n" +
            "     Offset numeric(10,5) not null,\n" +
            "     Multiplier numeric(10,5) not null,\n" +
            "     TimeFromConnection numeric(10) not null,\n" +
            "     constraint FKContains primary key (PeripheralID, Timestamp, TimeFromConnection),\n" +
            "     foreign key (PeripheralID, Timestamp) references Connessione);";

    /**
     * Creazione di un nuovo DatabaseHanlder
     * @param context context della view in cui viene creato
     */
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.i("DBHanlder", "Creating tables...");
        sqLiteDatabase.execSQL(QUERY_CREATE_CONNECTIONS);
        sqLiteDatabase.execSQL(QUERY_CREATE_VALUES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS Connection");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS ReadValue");
        onCreate(sqLiteDatabase);
    }
}
