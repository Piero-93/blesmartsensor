package it.pierobiagini.blesmartsensor.app.bluetooth;

import android.bluetooth.BluetoothDevice;

/**
 * Modella un observer del BluetoothScanner
 */
public interface BluetoothScannerObserver {

    /**
     * Rilevamento di un dispositivo
     * @param device dispositivo rilevato
     */
    void deviceFound(BluetoothDevice device);

    /**
     * Termine della scansione
     */
    void scanningFinished();

    /**
     * Inizio della scansione
     */
    void scanningStarted();

}
