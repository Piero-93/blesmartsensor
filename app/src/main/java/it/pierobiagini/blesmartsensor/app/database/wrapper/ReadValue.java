package it.pierobiagini.blesmartsensor.app.database.wrapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Modella un record del database della tabella "Values"
 */
public class ReadValue implements Serializable {
    private static final long serialVersionUID = 0L;

    //Nomi dei campi per la rappresentazione JSON
    public static final String PERIPHERAL_ID = "peripheralId";
    public static final String TIMESTAMP = "timestamp";
    public static final String VALUE = "rawValue";
    public static final String MULTIPLIER = "multiplier";
    public static final String OFFSET = "offset";
    public static final String TIME_FROM_CONNECTION = "timeFromConnection";

    private String peripheralID = "";
    private MyDate timestamp = new MyDate();
    private int value = 0;
    private double offset = 0;
    private double multiplier = 1;
    private int timeFromConnection = 0;

    /**
     * Creazione di un readValue inizializzando tutti i valori manualmente
     * @param peripheralID id della periferica
     * @param timestamp oggetto di tipo {@link String} rappresentante il timestamp della connessione
     * @param value valore letto
     * @param offset valore di scostamento
     * @param multiplier valore del moltiplicatore
     * @param timeFromConnection tempo trascorso dalla connessione
     */
    public ReadValue(String peripheralID, String timestamp, int value, double offset, double multiplier, int timeFromConnection) {
        this.peripheralID = peripheralID;
        this.timestamp = MyDate.convertStringTimestampToDate(timestamp);
        this.value = value;
        this.offset = offset;
        this.multiplier = multiplier;
        this.timeFromConnection = timeFromConnection;
    }

    /**
     * Creazione di un readValue inizializzando tutti i valori manualmente
     * @param peripheralID id della periferica
     * @param timestamp oggetto di tipo {@link MyDate} rappresentante il timestamp della connessione
     * @param value valore letto
     * @param offset valore di scostamento
     * @param multiplier valore del moltiplicatore
     * @param timeFromConnection tempo trascorso dalla connessione
     */
    public ReadValue(String peripheralID, MyDate timestamp, int value, double offset, double multiplier, int timeFromConnection) {
        this.peripheralID = peripheralID;
        this.timestamp = timestamp;
        this.value = value;
        this.offset = offset;
        this.multiplier = multiplier;
        this.timeFromConnection = timeFromConnection;
    }

    /**
     * Creazione di un readValue inizializzando tutti i valori manualmente
     * @param peripheralID id della periferica
     * @param timestamp variabile di tipo primitivo long rappresentante il timestamp della connessione
     * @param value valore letto
     * @param offset valore di scostamento
     * @param multiplier valore del moltiplicatore
     * @param timeFromConnection tempo trascorso dalla connessione
     */
    public ReadValue(String peripheralID, long timestamp, int value, double offset, double multiplier, int timeFromConnection) {
        this.peripheralID = peripheralID;
        this.timestamp.setTime(timestamp);
        this.value = value;
        this.offset = offset;
        this.multiplier = multiplier;
        this.timeFromConnection = timeFromConnection;
    }

    /**
     * Ottenimento dell'id della periferica
     * @return l'id della periferica
     */
    public String getPeripheralID() {
        return peripheralID;
    }

    /**
     * Ottenimento del timestamp della connessione
     * @return il timestamp della connessione
     */
    public MyDate getTimestamp() {
        return timestamp;
    }

    /**
     * Ottenimento del valore letto
     * @return il valore leto
     */
    public int getValue() {
        return value;
    }

    /**
     * Ottenimento del valore di scostamento
     * @return il valore di scostamento
     */
    public double getOffset() {
        return offset;
    }

    /**
     * Ottenumento del valore del moltiplicatore
     * @return il valore del moltiplicatore
     */
    public double getMultiplier() {
        return multiplier;
    }

    /**
     * Ottenimento del tempo trascorso dall'inizio della connessione
     * @return il tempo trascorso dall'inizio della connessione
     */
    public int getTimeFromConnection() {
        return timeFromConnection;
    }

    /**
     * Coversione dell'oggetto nella sua rappresentazione JSON
     * @return una rappresentazione JSON dell'oggetto
     * @throws JSONException
     */
    public JSONObject toJSONObject() throws JSONException {
        return new JSONObject()
                .put(VALUE, value)
                .put(MULTIPLIER, multiplier)
                .put(OFFSET, offset)
                .put(TIME_FROM_CONNECTION, timeFromConnection);
    }

    @Override
    public String toString() {
        return "ReadValue{" +
                "peripheralID='" + peripheralID + '\'' +
                ", timestamp=" + timestamp.toVisualizableString() +
                ", value=" + value +
                ", offset=" + offset +
                ", multiplier=" + multiplier +
                ", timeFromConnection=" + timeFromConnection +
                '}';
    }
}
