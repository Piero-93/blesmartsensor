package it.pierobiagini.blesmartsensor.app.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import it.pierobiagini.blesmartsensor.app.database.wrapper.MyDate;

import java.sql.SQLException;

/**
 * Modella un adapter per il database
 */
public class DatabaseAdapter {
    private static final String LOG_TAG = DatabaseAdapter.class.getSimpleName();

    private Context context;
    private SQLiteDatabase db;
    private DatabaseHandler dbHandler;

    //Tabelle del database
    private static final String TABLE_CONNECTIONS = "Connections";
    private static final String TABLE_VALUES = "ReadValues";

    //Campi della tabella "Connections"
    public static final String CONNECTION_PERIPHERAL_ID = "PeripheralID";
    public static final String CONNECTION_PERIPHERAL_NAME = "PeripheralName";
    public static final String CONNECTION_TIMESTAMP = "Timestamp";

    //Campi della tabella "ReadValues"
    public static final String VALUES_PERIPHERAL_ID = "PeripheralID";
    public static final String VALUES_TIMESTAMP = "Timestamp";
    public static final String VALUES_VALUE = "Value";
    public static final String VALUES_OFFSET = "Offset";
    public static final String VALUES_MULTIPLIER = "Multiplier";
    public static final String VALUES_TIME_FROM_CONNECTION = "TimeFromConnection";

    /**
     * Creazione di un nuovo database adapter
     * @param context contesto della view in cui viene creato
     */
    public DatabaseAdapter(Context context) {
        this.context = context;
    }

    /**
     * Apertura della connessione con il database
     * @return
     * @throws SQLException
     */
    public void open() throws SQLException {
        dbHandler = new DatabaseHandler(context);
        db = dbHandler.getWritableDatabase();
    }

    /**
     * Chiusura della connessione con il database
     */
    public void close() {
        dbHandler.close();
    }

    /**
     * Creazione di un nuovo record della tabella "Connections"
     * @param peripheralID id del device
     * @param peripheralName nome del device
     * @param timestamp data e ora di connessione
     * @return il record creato
     */
    private ContentValues createConnectionsContentValues(String peripheralID, String peripheralName, String timestamp) {
        ContentValues values = new ContentValues();
        values.put(CONNECTION_PERIPHERAL_ID, peripheralID);
        values.put(CONNECTION_PERIPHERAL_NAME, peripheralName);
        values.put(CONNECTION_TIMESTAMP, timestamp);
        return values;
    }

    /**
     * Creazione di un nuovo record della tabella "ReadValues"
     * @param peripheralID id del device
     * @param timestamp data e ora della connessione
     * @param value valore letto
     * @param offset valore di scostamento
     * @param multiplier valore del moltiplicatore
     * @param timeFromConnection tempo trascorso dal momento della connessione
     * @return il record creato
     */
    private ContentValues createReadValuesContentValues(String peripheralID, String timestamp, int value, double offset, double multiplier, int timeFromConnection) {
        ContentValues values = new ContentValues();
        values.put(VALUES_PERIPHERAL_ID, peripheralID);
        values.put(VALUES_TIMESTAMP, timestamp);
        values.put(VALUES_VALUE, value);
        values.put(VALUES_OFFSET, offset);
        values.put(VALUES_MULTIPLIER, multiplier);
        values.put(VALUES_TIME_FROM_CONNECTION, timeFromConnection);
        return values;
    }

    /**
     * Inserimento di un nuovo record nella tabella "Connections"
     * @param peripheralID id del device
     * @param peripheralName nome del device
     * @param timestamp data e ora di connessione (fromato {@link MyDate})
     * @return true se è stato aggiunto correttamente, altrimenti false
     */
    public boolean createConnection(String peripheralID, String peripheralName, MyDate timestamp) {
        ContentValues initialValues = createConnectionsContentValues(peripheralID, peripheralName, timestamp.toStringTimestamp());
        return db.insertOrThrow(TABLE_CONNECTIONS, null, initialValues) > 0;
    }

    /**
     * Inserimento di un nuovo record nella tabella "Connections" generando automaticamente il timestamp di connessione
     * @param peripheralID id del device
     * @param peripheralName nome del device
     * @return true se è stato aggiunto correttamente, altrimenti false
     */
    public boolean createConnectionWithAutomaticTimestamp(String peripheralID, String peripheralName) {
        ContentValues initialValues = createConnectionsContentValues(peripheralID, peripheralName, (new MyDate().toStringTimestamp()));
        return db.insertOrThrow(TABLE_CONNECTIONS, null, initialValues) > 0;
    }

    /**
     * Inserimento di un nuovo record nella tabella "ReadValues"
     * @param peripheralID id del device
     * @param timestamp data e ora della connessione (formato {@link String})
     * @param value valore letto
     * @param offset valore di offset
     * @param multiplier valore del moltiplicatore
     * @param timeFromConnection tempo trascorso dalla connessione
     * @return true se è stato aggiunto correttamente, altrimenti false
     */
    public boolean createValue(String peripheralID, String timestamp, int value, double offset, double multiplier, int timeFromConnection) {
        ContentValues initialValues = createReadValuesContentValues(peripheralID, timestamp, value, offset, multiplier, timeFromConnection);
        return db.insertOrThrow(TABLE_VALUES, null, initialValues) > 0;
    }

    /**
     * Inserimento di un nuovo record nella tabella "ReadValues"
     * @param peripheralID id del device
     * @param timestamp data e ora della connessione (formato {@link MyDate})
     * @param value valore letto
     * @param offset valore di offset
     * @param multiplier valore del moltiplicatore
     * @param timeFromConnection tempo trascorso dalla connessione
     * @return true se è stato aggiunto correttamente, altrimenti false
     */
    public boolean createValue(String peripheralID, MyDate timestamp, int value,double offset, double multiplier, int timeFromConnection) {
        ContentValues initialValues = createReadValuesContentValues(peripheralID, timestamp.toStringTimestamp(), value, offset, multiplier, timeFromConnection);
        return db.insertOrThrow(TABLE_VALUES, null, initialValues) > 0;
    }

    /**
     * Ottiene tutti i record della tabella "Connections"
     * @return un {@link Cursor} contenente i record della tabella "Connections"
     */
    public Cursor fetchAllConnections() {
        return db.query(TABLE_CONNECTIONS, new String[]{CONNECTION_PERIPHERAL_ID, CONNECTION_PERIPHERAL_NAME, CONNECTION_TIMESTAMP}, null, null, null, null, null);
    }

    /**
     * Ottiene tutti i record della tabella "ReadValues" filtrando per connessione
     * @param peripheralID id del device
     * @param timestamp data e ora della connessione (formato {@link MyDate})
     * @return un {@link Cursor} contenente i record ottenuti
     */
    public Cursor fetchAllReadValuesFileringByConnections(String peripheralID, MyDate timestamp) {
        return db.query(TABLE_VALUES, new String[]{VALUES_PERIPHERAL_ID, VALUES_TIMESTAMP, VALUES_VALUE, VALUES_OFFSET, VALUES_MULTIPLIER, VALUES_TIME_FROM_CONNECTION},
                VALUES_PERIPHERAL_ID + " LIKE '" + peripheralID + "' AND " + VALUES_TIMESTAMP + " LIKE '" + timestamp.toStringTimestamp() + "'", null, null, null, null, null);

    }

    /**
     * Cancella i dati relativi ad una connessione (record rappresentante la connessione e tutti i record rappresentanti i valori)
     * @param peripheralID id della periferica
     * @param timestamp data e ora della connessione
     * @return true se sono stati eliminati correttamante, altrimenti false;
     */
    public boolean deleteConnetionAndRelatedValues(String peripheralID, MyDate timestamp) {
        boolean result = false;
        Cursor c = db.query(TABLE_CONNECTIONS, new String[]{CONNECTION_PERIPHERAL_ID, CONNECTION_TIMESTAMP}, CONNECTION_PERIPHERAL_ID + " LIKE '" + peripheralID + "'", null, null, null, null);
        c.moveToNext();
        result = db.delete(TABLE_VALUES, VALUES_PERIPHERAL_ID + " LIKE '" + peripheralID + "' AND "+ VALUES_TIMESTAMP + " LIKE '" + timestamp.toStringTimestamp() + "'", null) > 0;
        result = result || db.delete(TABLE_CONNECTIONS, CONNECTION_PERIPHERAL_ID + " LIKE '" + peripheralID + "' AND "+ CONNECTION_TIMESTAMP + " LIKE '" + timestamp.toStringTimestamp() + "'", null) > 0;
        return result;
    }


}
