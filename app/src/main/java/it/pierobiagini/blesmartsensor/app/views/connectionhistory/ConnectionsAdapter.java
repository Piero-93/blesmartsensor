package it.pierobiagini.blesmartsensor.app.views.connectionhistory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import it.pierobiagini.blesmartsensor.app.R;
import it.pierobiagini.blesmartsensor.app.database.wrapper.Connection;

import java.util.ArrayList;
import java.util.List;

/**
 * Modella l'adapter per la list view contenente le connessioni
 */
public class ConnectionsAdapter extends BaseAdapter {

    private List<Connection> connectionList = new ArrayList<>();
    private Context context;

    /**
     * Creazione di un nuovo adapter
     * @param context context della view in cui viene creato
     */
    public ConnectionsAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return connectionList.size();
    }

    @Override
    public Object getItem(int position) {
        return position < getCount() ? connectionList.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).inflate(R.layout.list_connections, parent, false);
        }

        TextView name = (TextView) convertView.findViewById(R.id.labelName);
        name.setText(connectionList.get(position).getPeripheralName());

        TextView id = (TextView) convertView.findViewById(R.id.labelId);
        id.setText(connectionList.get(position).getPeripheralID());

        TextView date = (TextView) convertView.findViewById(R.id.labelDate);
        date.setText(connectionList.get(position).getTimestamp().toVisualizableString());

        return convertView;
    }

    /**
     * Aggiunta di un elemento alla lista
     * @param connection elemento da aggiungere alla lista
     */
    public void addItem(Connection connection) {
        connectionList.add(connection);
    }

    /**
     * Svuotamento della lista
     */
    public void clear() {
        connectionList.clear();
    }
}
