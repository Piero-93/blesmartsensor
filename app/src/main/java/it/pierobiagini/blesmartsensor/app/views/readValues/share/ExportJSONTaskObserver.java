package it.pierobiagini.blesmartsensor.app.views.readvalues.share;

import org.json.JSONObject;

/**
 * Modella un listener per il {@link ExportJSONTask}
 */
public interface ExportJSONTaskObserver {

    /**
     * Esportazione terminata con errori
     */
    void finishedWithErrors();

    /**
     * Esportazione terminata con successo
     * @param o oggetto JSON contenente i dati esportati
     */
    void finished(JSONObject o);

}
