package it.pierobiagini.blesmartsensor.app.views;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import it.pierobiagini.blesmartsensor.app.R;

/**
 * Created by Piero on 07/09/15.
 */
public class AppCompatActivityWithColouredStatusBar extends AppCompatActivity {

    /**
     * Impostazione del colore della status bar (da eseguire solo su Lollipop)
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected void setStatusBarColor() {
        getWindow().setStatusBarColor(getResources().getColor(R.color.dark_master));
    }
}
