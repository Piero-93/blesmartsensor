package it.pierobiagini.blesmartsensor.app.views.readvalues;

import android.annotation.TargetApi;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import it.pierobiagini.blesmartsensor.app.R;
import it.pierobiagini.blesmartsensor.app.database.DatabaseAdapter;
import it.pierobiagini.blesmartsensor.app.database.wrapper.MyDate;
import it.pierobiagini.blesmartsensor.app.database.wrapper.ReadValue;
import it.pierobiagini.blesmartsensor.app.views.readvalues.share.ExportJSONTask;
import it.pierobiagini.blesmartsensor.app.views.readvalues.share.ExportJSONTaskObserver;
import it.pierobiagini.blesmartsensor.app.views.readvalues.layout.SlidingTabLayout;
import it.pierobiagini.blesmartsensor.app.views.settings.SettingsActivity;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Modella la view che contiene le tab per la visualizzazione dei dati raccolti
 */
public class ReadValuesActivity extends AppCompatActivity implements ExportJSONTaskObserver {

    private List<ReadValue> readValues;

    private final DatabaseAdapter dbAdapter = new DatabaseAdapter(this);

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setStatusBarColor() {
        getWindow().setStatusBarColor(getResources().getColor(R.color.dark_master));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_values);

        //Creazione della toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        setStatusBarColor();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        //Recupero dei vallri dall'intent
        Intent inputIntent = getIntent();
        String peripheralID = inputIntent.getStringExtra(dbAdapter.VALUES_PERIPHERAL_ID);
        MyDate timestamp = (MyDate) inputIntent.getSerializableExtra(dbAdapter.VALUES_TIMESTAMP);

        Cursor fetchedValues = null;
        try {
            dbAdapter.open();
            fetchedValues = dbAdapter.fetchAllReadValuesFileringByConnections(peripheralID, timestamp);
        } catch (SQLException e) {
            Toast.makeText(this, R.string.db_connection_error, Toast.LENGTH_LONG);
            finish();
        }

        readValues = new ArrayList<>();
        while(fetchedValues.moveToNext()) {
            readValues.add(new ReadValue(fetchedValues.getString(0), fetchedValues.getString(1), fetchedValues.getInt(2),
                    fetchedValues.getDouble(3), fetchedValues.getDouble(4), fetchedValues.getInt(5)));
        }

        //Creazione del ViewPagerAdapter
        ViewPagerAdapter adapter =  new ViewPagerAdapter(getSupportFragmentManager(), new CharSequence[]{getString(R.string.chart), getString(R.string.table)}, 2, readValues);

        //Assegnamento del ViewPagerAdapter alla corretta view
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        //Assegnamento dello SlidingTabLayout
        SlidingTabLayout tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);

        //Impostazione del colore
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.button_material_light);
            }
        });

        tabs.setViewPager(pager);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_read_values, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent settings = new Intent(this, SettingsActivity.class);
            startActivity(settings);
            return true;
        } else if (id == R.id.action_share) {
            ReadValue[] array = new ReadValue[readValues.size()];
            readValues.toArray(array);
            new ExportJSONTask(this).execute(array);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void finishedWithErrors() {
        Toast.makeText(this, "An error occurred during sharing", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void finished(JSONObject o) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, o.toString());
        shareIntent.setType("text/plain");
        startActivity(shareIntent);
    }


}
