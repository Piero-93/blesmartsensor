package it.pierobiagini.blesmartsensor.app.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.os.Handler;
import android.util.Log;

/**
 * Modella il BluetoothScanner, che si occupa della gestione della scansione per la ricerca di nuovi dispositivi
 *
 * @author Biagini Piero
 */
public final class BluetoothScanner {

    //Riferimento al BluetoothAdapter
    private static BluetoothAdapter bluetoothAdapter;

    //Parametri di scansione
    private static boolean scanning = false;
    private static final long SCAN_PERIOD = 30000; //millisecondi

    //Observer
    private static BluetoothScannerObserver scanner = null;

    //Callback della scansione
    private static final BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {
        /**
         * Rilevamento di un dispositivo
         * @param device dispositivo rilevato
         * @param rssi
         * @param scanRecord
         */
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
            if (scanner != null) {
                scanner.deviceFound(device);
            }
        }
    };

    /**
     * Costruttore privato. Classe statica.
     */
    private BluetoothScanner() {}

    /**
     * Controllo che il Bluetooth sia abilitato
     * @return true se è abilitato, altrimenti false
     */
    public static boolean checkBluetoothTurnedOn() {
        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            return false;
        }
        return true;
    }

    /**
     * Inizializzazione del BluetoothAdapter
     * @param bluetoothManager riferimento al BluetoothManager
     * @return true se è stato inizializzato correttamente, altrimenti false
     */
    public static boolean initialize(BluetoothManager bluetoothManager) {
        bluetoothAdapter = bluetoothManager.getAdapter();

        return checkBluetoothTurnedOn();
    }

    /**
     * Esecuzione della scansione per un tempo pari a {@link BluetoothScanner#SCAN_PERIOD} millisecondi
     */
    public static void scan() {

        if(scanning) {
            bluetoothAdapter.stopLeScan(leScanCallback);
            bluetoothAdapter.startLeScan(leScanCallback);
        }

        scanning = true;
        scanner.scanningStarted();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(scanning) {
                    scanning = false;
                    scanner.scanningFinished();
                    Log.i("Timeout occurred", "Stop scan");
                    bluetoothAdapter.stopLeScan(leScanCallback);
                }
            }
        }, SCAN_PERIOD);

        scanning = true;
        bluetoothAdapter.startLeScan(leScanCallback);
    }

    /**
     * Impostazione dell'observer
     * @param scanner observer da notificare
     */
    public static void setScanner(BluetoothScannerObserver scanner) {
        BluetoothScanner.scanner = scanner;
    }

    /**
     * Terminazione della scansione
     */
    public static void stopScan() {
        scanning = false;
        bluetoothAdapter.stopLeScan(leScanCallback);
    }
}
