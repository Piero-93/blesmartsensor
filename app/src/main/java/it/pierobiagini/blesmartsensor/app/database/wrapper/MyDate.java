package it.pierobiagini.blesmartsensor.app.database.wrapper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Pattern;

/**
 * Modella un {@link Date} di cui è possibile ottenere rappresentazioni in forma di {@link String}
 */
public class MyDate extends Date {

    /**
     * Creazione di una nuova data inizializzandola con l'orario  corrente
     */
    public MyDate() {
        super();
    }

    /**
     * Creazione di una nuova data
     * @param date l'orario di inizializzazione della data
     */
    public MyDate(Date date) {
        super(date.getTime());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        MyDate date;
        try {
            date = (MyDate) o;
        } catch (ClassCastException e) {
            return false;
        }

        Calendar calendarA = GregorianCalendar.getInstance();
        calendarA.setTime(date);

        Calendar calendarB = GregorianCalendar.getInstance();
        calendarB.setTime(this);

        if(calendarA.get(Calendar.YEAR) == calendarB.get(Calendar.YEAR) &&
                calendarA.get(Calendar.MONTH) == calendarB.get(Calendar.MONTH) &&
                calendarA.get(Calendar.DAY_OF_MONTH) == calendarB.get(Calendar.DAY_OF_MONTH) &&
                calendarA.get(Calendar.HOUR_OF_DAY) == calendarB.get(Calendar.HOUR_OF_DAY) &&
                calendarA.get(Calendar.MINUTE) == calendarB.get(Calendar.MINUTE)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Controllo della validità di una {@link String} rappresentante una data
     * @param candidate stringa da controllare
     * @return true se la stringa è valida, altrimenti false
     */
    protected static boolean isValidTimestamp(String candidate) {
        return Pattern.matches("\\d{4}-\\d{2}-\\d{2},\\d{2}:\\d{2}:\\d{2}", candidate);
    }

    /**
     * Conversione di una {@link String} rappresentante una data in un oggetto di tipo {@link MyDate}
     * @param timestamp {@link String} da convertire
     * @return un oggetto di tipo {@link MyDate} corrispondente alla conversione della {@link String}
     */
    public static MyDate convertStringTimestampToDate(String timestamp) {
        Date resultDate = null;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss");
        try {
            resultDate = dateFormat.parse(timestamp);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new MyDate(resultDate);
    }

    /**
     * Ottenimento del corrispondente timestamp in formato "yyyy-MM-dd,HH:mm"
     * @return
     */
    public String toStringTimestamp() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss");
        return dateFormat.format(this);
    }

    /**
     * Ottenimento del corrispondente timestamp in formato "dd/MM/yyyy, HH:mm"
     * @return
     */
    public String toVisualizableString() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy, HH:mm");
        return dateFormat.format(this);
    }
}
