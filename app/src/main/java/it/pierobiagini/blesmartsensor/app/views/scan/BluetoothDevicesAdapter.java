package it.pierobiagini.blesmartsensor.app.views.scan;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import it.pierobiagini.blesmartsensor.app.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Modella l'adapter per la list view contenente i dispositivi rilevati
 */
public class BluetoothDevicesAdapter extends BaseAdapter {

    private List<BluetoothDevice> devicesList = new ArrayList<>();
    private Context context;

    /**
     * Creazione di un nuovo adapter
     * @param context context della view in cui viene creato
     */
    public BluetoothDevicesAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return devicesList.size();
    }

    @Override
    public Object getItem(int position) {
        return position < getCount() ? devicesList.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).inflate(R.layout.list_scan, parent, false);
        }

        TextView name = (TextView) convertView.findViewById(R.id.labelName);
        name.setText(devicesList.get(position).getName());

        TextView id = (TextView) convertView.findViewById(R.id.labelId);
        id.setText(devicesList.get(position).getAddress());

        return convertView;
    }

    /**
     * Aggiunta di un elemento alla lista
     * @param newDevice elemento da aggiungere alla lista
     */
    public void addItem(BluetoothDevice newDevice) {
        devicesList.add(newDevice);
    }

    /**
     * Svuotamento della lista
     */
    public void clear() {
        devicesList.clear();
    }
}
