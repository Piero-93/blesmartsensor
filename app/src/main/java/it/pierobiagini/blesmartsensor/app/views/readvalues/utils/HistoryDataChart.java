package it.pierobiagini.blesmartsensor.app.views.readvalues.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.preference.PreferenceManager;
import android.widget.LinearLayout;
import it.pierobiagini.blesmartsensor.app.R;
import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

/**
 * Modella il grafico visualizzato nella view di visualizzazione dei dati memorizzati.
 * Viene notificata quando cambiano valori di shared preferences legati a questa visualizzazione.
 */
public final class HistoryDataChart implements SharedPreferences.OnSharedPreferenceChangeListener {

    private final static double TIME_INTERVAL = 0.5;

    private XYMultipleSeriesDataset dataSet = new XYMultipleSeriesDataset();
    private XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
    private XYSeries dataSeries;
    private XYSeriesRenderer currentRenderer;
    private GraphicalView chart;
    private double counter;
    private double max = 0;

    private Context context;

    private static int xZoomRate = 10;
    private static int yZoomRate = 10;

    /**
     * Inizializzazione del grafico
     * @param context context della view in cui dovrà essere inserito il grafico
     */
    private void initialize(Context context) {
        dataSeries = new XYSeries("Data");
        dataSet.addSeries(dataSeries);
        currentRenderer = new XYSeriesRenderer();
        renderer.addSeriesRenderer(currentRenderer);

        //Graphical details
        currentRenderer.setPointStyle(PointStyle.DIAMOND);
        currentRenderer.setFillPoints(true);
        currentRenderer.setLineWidth(5);
        renderer.setPointSize(10);
        renderer.setMarginsColor(context.getResources().getColor(R.color.background_material_light));
        renderer.setBackgroundColor(context.getResources().getColor(R.color.background_material_dark));
        renderer.setLabelsTextSize(40);
        renderer.setAxisTitleTextSize(30);
        renderer.setYTitle("Value");
        renderer.setYAxisAlign(Paint.Align.LEFT, 0);
        renderer.setYLabelsAlign(Paint.Align.RIGHT, 0);
        renderer.setYLabelsAngle(90);
        renderer.setYLabelsColor(0, context.getResources().getColor(R.color.abc_primary_text_material_light));
        renderer.setXTitle("Time(s)");
        renderer.setXLabelsColor(context.getResources().getColor(R.color.abc_primary_text_material_light));
        renderer.setGridColor(context.getResources().getColor(R.color.background_floating_material_dark));
        renderer.setLabelsColor(context.getResources().getColor(R.color.abc_primary_text_material_light));
        renderer.setShowGrid(true);
        renderer.setZoomEnabled(true, true);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.registerOnSharedPreferenceChangeListener(this);
        xZoomRate = Integer.parseInt(sp.getString(context.getString(R.string.x_zoom_rate_property), "10"));
        yZoomRate = Integer.parseInt(sp.getString(context.getString(R.string.y_zoom_rate_property), "10"));
    }

    /**
     * Sostituzione del grafico al {@link LinearLayout}
     * @param layout {@link LinearLayout} da sostituire
     */
    public void setChartInLayout(LinearLayout layout) {
        if(chart == null) {
            context = layout.getContext();
            this.initialize(layout.getContext());
            chart = ChartFactory.getLineChartView(layout.getContext(), dataSet, renderer);
            layout.addView(chart);
        } else {
            chart.repaint();
        }
    }

    /**
     * Aggiunta dei dati nel grafico
     * @param data valore da aggiungere
     * @param multiplier valore del moltiplicatore
     * @param offset valore di scostamento
     * @param elapsedTime tempo trascorso dalla connessione
     */
    public void addData(int data, double multiplier, double offset, int elapsedTime) {
        counter += ((elapsedTime/1000) - counter) * TIME_INTERVAL;
        dataSeries.add(counter, data * multiplier + offset);
        if((data * multiplier + offset) > max) {
            max = data * multiplier + offset;
        }
        renderer.setPanLimits(new double[]{-10, counter + (counter * 15 / 100), -10, max + (max * 15 / 100)});
        renderer.setZoomLimits(new double[]{-10, counter + (counter * 15 / 100), -10, max + (max * 15 / 100)});
    }


    /**
     * Centramento del grafico in modo da visualizzare tutti i valori
     */
    public void center() {
        renderer.setXAxisMin(-10);
        renderer.setXAxisMax(counter + (counter * 15 / 100));
        renderer.setYAxisMin(-10);
        renderer.setYAxisMax(max + (max * 15 / 100));
        chart.repaint();
    }

    /**
     * Zoom verticale in avanti
     */
    public void verticalZoomIn() {
        if(renderer.getYAxisMin() + yZoomRate < renderer.getYAxisMax()) {
            renderer.setYAxisMin(renderer.getYAxisMin() + yZoomRate);
            renderer.setYAxisMax(renderer.getYAxisMax() - yZoomRate);
            chart.repaint();
        }
    }

    /**
     * Zoom verticale all'indietro
     */
    public void verticalZoomOut() {
        renderer.setYAxisMin(renderer.getYAxisMin() - yZoomRate);
        renderer.setYAxisMax(renderer.getYAxisMax() + yZoomRate);
        chart.repaint();
    }

    /**
     * Zoom orizzontale in avanti
     */
    public void horizontalZoomIn() {
        if(renderer.getXAxisMin() + xZoomRate < renderer.getXAxisMax()) {
            renderer.setXAxisMin(renderer.getXAxisMin() + xZoomRate);
            renderer.setXAxisMax(renderer.getXAxisMax() - xZoomRate);
            chart.repaint();
        }
    }

    /**
     * Zoom verticale all'indietro
     */
    public void horizontalZoomOut() {
        renderer.setXAxisMin(renderer.getXAxisMin() - xZoomRate);
        renderer.setXAxisMax(renderer.getXAxisMax() + xZoomRate);
        chart.repaint();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        if(s.compareTo(context.getString(R.string.x_zoom_rate_property)) == 0) {
            xZoomRate = Integer.parseInt(sharedPreferences.getString(context.getString(R.string.x_zoom_rate_property), "10"));
        }

        if(s.compareTo(context.getString(R.string.y_zoom_rate_property)) == 0) {
            yZoomRate = Integer.parseInt(sharedPreferences.getString(context.getString(R.string.y_zoom_rate_property), "10"));
        }
    }
}
