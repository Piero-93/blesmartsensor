package it.pierobiagini.blesmartsensor.app.views.readvalues.share;

import android.os.AsyncTask;
import it.pierobiagini.blesmartsensor.app.database.wrapper.ReadValue;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Modella un task ({@link AsyncTask}) in grado di esportare una serie di valori in JSON
 */
public class ExportJSONTask extends AsyncTask<ReadValue, Double, JSONObject> {
    public static final String VALUE_COUNT = "valueCount";
    public static final String VALUES = "values";

    private final ExportJSONTaskObserver observer;

    /**
     * Creazione di un nuovo task
     * @param observer observer che verrà notificato al termine delle operazioni
     */
    public ExportJSONTask(ExportJSONTaskObserver observer) {
        this.observer = observer;
    }

    @Override
    protected JSONObject doInBackground(ReadValue... readValues) {
        JSONArray valuesJSONArray = new JSONArray();
        for (ReadValue readValue : readValues) {
            try {
                valuesJSONArray.put(readValue.toJSONObject());
            } catch (JSONException e) {
                return null;
            }
        }
        try {
            return new JSONObject()
                    .put(ReadValue.PERIPHERAL_ID, readValues[0].getPeripheralID())
                    .put(ReadValue.TIMESTAMP, readValues[0].getTimestamp())
                    .put(VALUE_COUNT, readValues.length)
                    .put(VALUES, valuesJSONArray);
        } catch (JSONException e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
        if (jsonObject == null) {
            observer.finishedWithErrors();
        } else {
            observer.finished(jsonObject);
        }
    }
}
