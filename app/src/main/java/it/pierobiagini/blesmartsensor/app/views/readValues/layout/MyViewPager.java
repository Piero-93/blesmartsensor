package it.pierobiagini.blesmartsensor.app.views.readvalues.layout;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Modella un ViewPager in cui è possibile ridefinire il comportamento delle varie azioni di tocco
 */
public class MyViewPager extends ViewPager {

    GestureDetectorCompat listener;

    /**
     * Creazione di un nuovo MyViewPager
     * @param context context della view in cui verrà posizioneto
     */
    public MyViewPager(Context context) {
        super(context);
    }

    /**
     * Creazione di un nuovo MyViewPager
     * @param context context della view in cui verrà posizioneto
     * @param attrs attributi
     */
    public MyViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return false;
    }
}
